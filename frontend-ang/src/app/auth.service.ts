import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { shareReplay, tap } from 'rxjs';
import { WebRequestService } from './pages/task-view/web-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private WebService: WebRequestService, private router: Router, private http: HttpClient) { }

  login(email:string, password: string){
    return this.WebService.login(email, password).pipe(
      shareReplay(), 
      tap((res: HttpResponse<any>) => {
        this.setSession(res.body.user._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
        console.log("Logged In")
        
      })
    )
  }

  getUser() {
    return this.WebService.get('api/userInfo/me')
  }

  signup(email:string, name: string, password: string){
    return this.WebService.signup(email, name, password).pipe(
      shareReplay(), 
      tap((res: HttpResponse<any>) => {
        this.setSession(res.body.user._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
        console.log("Successfully sign up!")
        
      })
    )
  }



  logout(){
    this.removeSession();

    this.router.navigateByUrl('login');
  }
  getRefreshToken() {
    return localStorage.getItem('x-refresh-token')
  }

  getUserId() {
    return localStorage.getItem('user-id')
  }

  getAccessToken() {
    return localStorage.getItem('x-access-token');
  }

  setAccessToken(accessToken:string) {
    localStorage.setItem('x-access-token', accessToken)
  }

  private setSession(userId: string, accessToken: any, refreshToken: any) {
    localStorage.setItem('user-id', userId);
    localStorage.setItem('x-access-token', accessToken);
    localStorage.setItem('x-refresh-token', refreshToken);
  }
  private removeSession() {
    localStorage.removeItem('user-id');
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-refresh-token');
  }

  getNewAccessToken() {
    return this.http.get(`${this.WebService.ROOT_URL}/api/user/me/access-token`, {
      headers: new HttpHeaders({
        'x-refresh-token': this.getRefreshToken()!,
        '_id': this.getUserId()!
      }),
      observe: 'response'
    }).pipe(
      tap((res: HttpResponse<any>) => {
        this.setAccessToken(res.headers.get('x-access-token')!)
      })
    )
  }

}
