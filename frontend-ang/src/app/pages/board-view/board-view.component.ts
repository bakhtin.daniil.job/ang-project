import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { TaskService } from '../task-view/task.service';
@Component({
  selector: 'app-board-view',
  templateUrl: './board-view.component.html',
  styleUrls: ['./board-view.component.scss']
})
export class BoardViewComponent implements OnInit {

  lists: any;
  tasks: any;
  tasksInProgress: any;
  completedTask: any;
  userName: any;
  selectedListId: string | undefined;

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if(params["listId"]) {
          this.selectedListId = params["listId"];
            this.taskService.getTasks(params["listId"]).subscribe((task: any) => {
             this.tasks = task.task.filter((item:any) => item.status === 'todo');
            this.tasksInProgress = task.task.filter((item:any) => item.status === 'progress');
            this.completedTask = task.task.filter((item:any) => item.status === 'completed');
            console.log(this.tasks, this.tasksInProgress, this.completedTask)
            // this.tasks = task.task.filter((item: any) => item.completed === false)
            // this.completedTask = task.task.filter((item: any) => item.completed === true)
          })  
        } else {
          this.tasks = undefined;
        }
      }
    )
    this.authService.getUser().subscribe((userName: any) => {
      this.userName = userName.name;
    })

      this.taskService.getLists().subscribe((lists: any) => {
          this.lists = lists.list;
          console.log(lists)
      })
  }

  
  // onTaskClick(task: any) {
  //   this.taskService.complete(task).subscribe(() => {
  //     task.completed = !task.completed;
  //     this.ngOnInit()
  //   })
  // }
  onTaskClick(task:any){
    if(task.status === 'todo'){
      this.taskService.status(task, 'progress').subscribe(() => {
            this.ngOnInit()
      })
    }
    if(task.status === 'progress'){
      this.taskService.status(task, 'completed').subscribe(() => {
        this.ngOnInit()
  })
    }
  }
  
  sortTaskByName(){
    this.tasks.sort((a:any, b:any) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1)
    this.tasksInProgress.sort((a:any, b:any) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1)
    this.completedTask.sort((a:any, b:any) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1)
  }

  sortTaskByDate(){
    this.tasks.sort((a: any, b: any) => {
      return <any>new Date(b.createDate) - <any>new Date(a.createDate);
    });
    this.tasksInProgress.sort((a: any, b: any) => {
      return <any>new Date(b.createDate) - <any>new Date(a.createDate);
    });
    this.completedTask.sort((a: any, b: any) => {
      return <any>new Date(b.createDate) - <any>new Date(a.createDate);
    });
  }

  filterTask(name:string){
    if(name === ''){
      this.ngOnInit()
    } else {
      this.tasks = this.tasks.filter((res:any) => {
        return res.title.toLocaleLowerCase().match(name.toLocaleLowerCase())
      })
      this.tasksInProgress = this.tasksInProgress.filter((res:any) => {
        return res.title.toLocaleLowerCase().match(name.toLocaleLowerCase())
      })
      this.completedTask = this.completedTask.filter((res:any) => {
        return res.title.toLocaleLowerCase().match(name.toLocaleLowerCase())
      })
    }
  }


  onDeleteTaskClick(id: string){
    this.taskService.deleteTask(this.selectedListId!, id).subscribe((res: any) => {
      this.tasks = this.tasks.filter((val: { _id: string; }) => val._id !== id)
      this.ngOnInit()
      console.log(res)
    })
  }

  onLogout(){
    this.authService.logout()
  }


  drop(event: CdkDragDrop<any>) {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    )
    if(event.container.data[event.currentIndex].status !== 'todo'){
    this.taskService.status(event.container.data[event.currentIndex], 'todo').subscribe(() => {})
    }
  }

  dropProgress(event: CdkDragDrop<any>) {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    )
    if(event.container.data[event.currentIndex].status !== 'progress'){
    this.taskService.status(event.container.data[event.currentIndex], 'progress').subscribe(() => {})
    }
  }
  dropCompleted(event: CdkDragDrop<any>) {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    )
    if(event.container.data[event.currentIndex].status !== 'completed'){
    this.taskService.status(event.container.data[event.currentIndex], 'completed').subscribe(() => {})
    }
  }
}
