import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { TaskService } from '../task-view/task.service';
@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss']
})
export class NewListComponent implements OnInit {

  listNameValid: boolean | undefined;
  constructor(private taskService: TaskService, private router: Router) { }

  ngOnInit(): void {
  }

  createNewList(title: string, description:string) {
    if(title === ''){
      this.listNameValid = true;
    } else {
      this.listNameValid = false;
    }

    if(!this.listNameValid){
    this.taskService.createList(title, description).subscribe((response: any) => {
      console.log(response)

      // this.router.navigate([ '/lists', response.listDoc._id ])
      this.router.navigate([ '/lists' ])
    })
    }
  }
}
