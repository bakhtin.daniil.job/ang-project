import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private werReqService: WebRequestService) { }

  createList(title: string, description:string){
   return this.werReqService.post('api/lists', { title, description })
  }
  updateList(id: string, title: string){
   return this.werReqService.patch(`api/lists/${id}`, { title })
  }

  updateTask(listId: string, taskId: string, title: string){
   return this.werReqService.patch(`api/lists/${listId}/tasks/${taskId}`, { title })
  }

  deleteList(id: string){
    return this.werReqService.delete(`api/lists/${id}`);
  }

  deleteTask(listId: string, taskId: string){
    return this.werReqService.delete(`api/lists/${listId}/tasks/${taskId}`);
  }

  getLists() {
    return this.werReqService.get('api/lists')
  }

  getTasks(listId: string) {
    return this.werReqService.get(`api/lists/${listId}/tasks`);
  }

  createTask(title: string, description: string, listId: string){
    return this.werReqService.post(`api/lists/${listId}/tasks`, { title, description })
   }

  complete(task: any) {
    return this.werReqService.patch(`api/lists/${task._listId}/tasks/${task._id}`, {
      completed: !task.completed
    });
  }
  status(task: any, status:string) {
    return this.werReqService.patch(`api/lists/${task._listId}/tasks/${task._id}`, {
      status: status
    })
  }
}
