import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { TaskService } from './task.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  lists: any;
  tasks: any;
  userName: any;
  selectedListId: string | undefined;

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if(params["listId"]) {
          this.selectedListId = params["listId"];
            this.taskService.getTasks(params["listId"]).subscribe((task: any) => {
              this.tasks = task.task;
          })  
        } else {
          this.tasks = undefined;
        }
      }
    )
    this.authService.getUser().subscribe((userName: any) => {
      this.userName = userName.name;
    })

      this.taskService.getLists().subscribe((lists: any) => {
          this.lists = lists.list;
          console.log(lists)
      })
    
  }


  onTaskClick(task: any) {
    this.taskService.complete(task).subscribe(() => {
      task.completed = !task.completed;
    })
  }
  
  sortListByName(){
    this.lists.sort((a:any,b:any) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1)
  }

  sortTaskByName(){
    this.tasks.sort((a:any, b:any) => a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1)
  }

  sortTaskByDate(){
    this.tasks.sort((a: any, b: any) => {
      return <any>new Date(b.createDate) - <any>new Date(a.createDate);
    });
  }

  sortListByDate(){
    this.lists.sort((a: any, b: any) => {
      return <any>new Date(b.createDate) - <any>new Date(a.createDate);
    });
  }

  sortListByNumberOfTasks(){
    this.lists.sort((a:any, b:any) => b.numberOfTasks - a.numberOfTasks)
  }

  filterList(name: string) {
    if(name === '') {
      this.ngOnInit()
    } else {
      this.lists = this.lists.filter((res:any) => {
        return res.title.toLocaleLowerCase().match(name.toLocaleLowerCase())
      })
    }
  }

  filterTask(name:string){
    if(name === ''){
      this.ngOnInit()
    } else {
      this.tasks = this.tasks.filter((res:any) => {
        return res.title.toLocaleLowerCase().match(name.toLocaleLowerCase())
      })
    }
  }

  onDeleteListClick(id: string){
    this.taskService.deleteList(id!).subscribe((res: any) => {
      this.ngOnInit();
      // this.router.navigate(['/lists']);
      console.log(res);
    })
  }

  onDeleteTaskClick(id: string){
    this.taskService.deleteTask(this.selectedListId!, id).subscribe((res: any) => {
      this.tasks = this.tasks.filter((val: { _id: string; }) => val._id !== id)
      console.log(res)
    })
  }

  onLogout(){
    this.authService.logout()
  }
}
