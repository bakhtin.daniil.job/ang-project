import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {

  invalidEmail: boolean | undefined;
  invalidUserName: boolean | undefined;
  invalidPassword: boolean | undefined;
  errorMassage: boolean | undefined;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {

  }

  onSignupButtonClicked(email:string, name: string, password: string) {
    if(!email.toLocaleLowerCase().match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
      this.invalidEmail = true;
    } else {
      this.invalidEmail = false;
    }
    if(name === ''){
      this.invalidUserName = true;
    } else {
      this.invalidUserName = false;
    }
    if(password === ''){
      this.invalidPassword = true;
    } else {
      this.invalidPassword = false;
    }
      if(!this.invalidEmail && !this.invalidUserName && !this.invalidPassword){
        this.authService.signup(email, name, password).subscribe(
          (res: HttpResponse<any>) => {
          if(res.status === 200) {
            this.router.navigate(['/login'])
          }
        },
        (error) => {
          this.errorMassage = true;
        })
      }
  }

}
