import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task-view/task.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  listId: any;
  listNameValid: boolean | undefined;
  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
         this.listId = params['listId']
         console.log(this.listId)
      }
    )

  }

  createNewTask(title: string, description: string){
    if(title === ''){
      this.listNameValid = true;
    } else {
      this.listNameValid = false;
    }
    if(!this.listNameValid){
    this.taskService.createTask(title, description, this.listId).subscribe((newTask: any) => {
      this.router.navigate(['../'], {relativeTo: this.route})
    })
  }
}
}
