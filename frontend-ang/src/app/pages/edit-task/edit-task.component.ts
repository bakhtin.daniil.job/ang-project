import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TaskService } from '../task-view/task.service';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {

  taskId: any;
  listId: any;
  taskValid: boolean | undefined;
  constructor(private route: ActivatedRoute, private taskService: TaskService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
          this.taskId = params["taskId"];
          this.listId = params["listId"];
      }
    )
  }

  updateTask(title: string) {
    if(title === ''){
      this.taskValid = true;
    } else {
      this.taskValid = false;
    }

    if(!this.taskValid){
    this.taskService.updateTask(this.listId, this.taskId, title).subscribe(() => {
      this.router.navigate(['/lists', this.listId])
    })
  }
  } 


}
