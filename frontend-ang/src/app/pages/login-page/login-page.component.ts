import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  emailValid: boolean | undefined;
  passwordValid: boolean | undefined;
  errorMassage: boolean | undefined;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
   
  }

  validationFunc(email:string, password:string){
    if(!email.toLocaleLowerCase().match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
      this.emailValid = true;
    } else {
      this.emailValid = false;
    }
    if(password === ''){
      this.passwordValid = true;
    } else {
      this.passwordValid = false;
    }
  }
  
  onLoginButtonClicked(email:string, password: string){
    // if(!email.toLocaleLowerCase().match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
    //   this.emailValid = true;
    // } else {
    //   this.emailValid = false;
    // }
    // if(password === ''){
    //   this.passwordValid = true;
    // } else {
    //   this.passwordValid = false;
    // }
    this.validationFunc(email, password);
    if(!this.emailValid && !this.passwordValid){
    this.authService.login(email, password).subscribe(
      (res: HttpResponse<any>) => {
      if(res.status === 200){
        this.router.navigate(['/lists'])
      } 
    },
    (error) => {
      this.errorMassage = true;
    }
    
    )
  }
  }
}