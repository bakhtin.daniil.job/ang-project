const { List } = require("../models/listModel");
const { Task } = require("../models/taskModel");

const getTasksOfList = async (req, res, next) => {
    try {
        const { listId } = req.params;
        if(listId){
            const tasks = await Task.find({ _listId: listId})
            .then((task) => {
                if(task !== null){
               return res.status(200).send({
                   message: 'Tasks was successfully received',
                   count: task.length,
                   task
               })
            } else {
                return res.status(400).send({
                    message: 'There is no task with this list Id'
                })
            }
            }).catch(err => {
                return res.status(400).send({
                   message: err
               })
            })
           } else {
              return res.status(403).json({ message: 'Task was not received' });
           }

    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}

const createTaskOfList = async(req, res, next) => {
    try {
        const flag = 1;
        const { listId } = req.params;
        const { title, description } = req.body;
        const findList = await List.findOne({_id: listId, _userId: req.user_id }).then(user => {
            if(user){
                return true
            } else {
                return false
            }
        });
        if(!findList){
            return res.status(400).send({
                message: 'There is no such list'
            })
        } 
        
        if(listId && title){
            let newTask = new Task({
                title: title,
                description: description,
                _listId: listId
            })
            await newTask
            .save()
            .then(newTaskDoc => {
               res.status(200).send({
                message: 'Task was created',
                newTaskDoc
               })
            })
            .then(() => {
                List.findOneAndUpdate({ _id: listId, _userId: req.user_id }, {
                    $inc: {
                     numberOfTasks: flag
                    },
                 },{
                     new: true
                    }).then((ress) => {
                     console.log(ress)
                 }).catch(err => {
                     console.log(err)
                 })
            })
           } else {
               return res.status(403).json({ message: 'Task was not received' });
           }
    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}

const updateTaskOfList = async (req, res, next) => {
    try {
        const { listId, taskId } = req.params;
        const findList = await List.findOne({
            _id:listId,
            _userId: req.user_id
        }).then(list => {
            if(list){
                return true
            } else {
                return false
            }
        })
      if(!findList) { 
        return res.status(400).send({
            message: 'There is no such list'
        })
       } 
        if(listId && taskId){
            await Task.findOneAndUpdate({ 
                _id: taskId,
                _listId: listId
            }, {
                $set: req.body
            }).then((respTask) => {
                if(respTask !== null){
                    return res.status(200).send({
                        message: 'Task was successfully changed'
                    })
                } else {
                    return res.status(403).json({ message: 'Task was not received' });
                }
            })
           } else {
            return res.status(403).json({ message: 'Id of List and Task is required' });
           }
    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}

const deleteTaskOfList = async (req, res, next) => {
    try {
        const flag = -1;
        const { listId, taskId } = req.params;
        const findList = await List.findOne({
            _id:listId,
            _userId: req.user_id
        }).then(list => {
            if(list){
                return true
            } else {
                return false
            }
        })
      if(!findList) { 
       return res.status(400).send({
            message: 'There is no such list'
        })
        }
        if(listId && taskId){
            await Task.findOneAndDelete({
                _id: taskId,
                _listId: listId
            })
            .then(deletedTask => {
                if(deletedTask !== null){
                   return res.status(200).send({
                        message: 'Task was successfully deleted'
                    })
                } else {
                   return res.status(403).json({ message: 'Task was not received' });
                }
            })
            .then(() => {
                    List.findOneAndUpdate({ _id: listId, _userId: req.user_id }, {
                        $inc: {
                         numberOfTasks: flag
                        },
                     },{
                         new: true
                        }).then((ress) => {
                         console.log(ress)
                     }).catch(err => {
                         console.log(err)
                     })
                })
        } else {
           return res.status(403).json({ message: 'Id of List and Task is required' });
        }
    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}

const findTaskOfList = async (req, res, next) => {
    try {
        const { listId, taskId } = req.params;
        const findList = await List.findOne({
            _id:listId,
            _userId: req.user_id
        }).then(list => {
            if(list){
                return true
            } else {
                return false
            }
        })
      if(!findList) { 
        return res.status(400).send({
            message: 'There is no such list'
        })
        }
        if(listId && taskId){
            await Task.findOne({
                _id: taskId,
                _listId: listId
            })
            .then(findTask => {
                if(findTask !== null){
                    return res.status(200).send({
                        findTask
                    })
                } else {
                    return res.status(403).json({ message: 'Task was not received' });
                }
            })

        } else {
            return res.status(403).json({ message: 'Id of List and Task is required' });
        }
         
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

module.exports = {
    getTasksOfList,
    createTaskOfList,
    updateTaskOfList,
    deleteTaskOfList,
    findTaskOfList
}