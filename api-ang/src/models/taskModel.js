const { Schema, model, default: mongoose } = require('mongoose');


const taskSchema = new Schema({
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    description: {
        type: String
    },
    _listId: {
        type: mongoose.Types.ObjectId,
        required: true 
    },
    completed: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        default: 'todo'
    },
    createDate: {
        type: String,
        default: new Date().toISOString()
    }
})


const Task = model('tasks', taskSchema);

module.exports = {
    Task
}