const { Schema, model, default: mongoose } = require('mongoose');


const listSchema = new Schema({
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    description: {
        type: String,
    },
    numberOfTasks: {
        type: Number,
        default: 0
    },
    _userId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    createDate: {
        type: String,
        default: new Date().toISOString()
    }
})

const List = model('lists', listSchema);


module.exports = {
    List
}