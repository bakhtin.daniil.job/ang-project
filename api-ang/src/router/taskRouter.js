const express = require('express');
const { getTasksOfList, createTaskOfList, updateTaskOfList, deleteTaskOfList, findTaskOfList } = require('../controllers/taskController');

const router = express.Router();


router.get('/:listId/tasks', getTasksOfList)

router.post('/:listId/tasks', createTaskOfList)

router.patch('/:listId/tasks/:taskId', updateTaskOfList)

router.delete('/:listId/tasks/:taskId', deleteTaskOfList)

router.get('/:listId/tasks/:taskId', findTaskOfList)
module.exports = {
    taskRouter: router
}